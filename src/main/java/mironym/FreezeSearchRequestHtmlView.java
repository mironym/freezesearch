/*
   Copyright 2012 Miroslav Hornik (Mironym)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package mironym;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.search.Searcher;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.util.DocumentHitCollector;
import com.atlassian.jira.issue.views.util.IssueWriterHitCollector;
import com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParams;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraVelocityUtils;

/**
 * Based on https://developer.atlassian.com/display/JIRADEV/Search+Request+View+Plugin+Module .
 * This class implements the core functionality of the Freeze Search Jira plugin. It relies on 3 velocity
 * templates (header, issue, suffix) and it is memory friendly (using HitCollector).
 * 
 *  See https://bitbucket.org/mironym/freezesearch/wiki/Home for more information.
 * 
 * @author Miroslav Hornik
 *
 */
public class FreezeSearchRequestHtmlView extends AbstractSearchRequestView {
	private final JiraAuthenticationContext authenticationContext;
    private final SearchProviderFactory searchProviderFactory;
    private final IssueFactory issueFactory;
    private final SearchProvider searchProvider;
 
    /**
     * The only constructor, just sets all its arguments to the private variables.
     * 
     * @param authenticationContext
     * @param searchProviderFactory
     * @param issueFactory
     * @param searchProvider
     */
    public FreezeSearchRequestHtmlView(JiraAuthenticationContext authenticationContext, SearchProviderFactory searchProviderFactory,
            IssueFactory issueFactory, SearchProvider searchProvider)
    {
        this.authenticationContext = authenticationContext;
        this.searchProviderFactory = searchProviderFactory;
        this.issueFactory = issueFactory;
        this.searchProvider = searchProvider;
    }
 
    /**
     * This is the core functionality - writes to writer the Html according to the header, singleissue and footer Velocity
     * templates.
     * 
     * It wraps any {@link IOException} and {@link SearchException} to the {@link RuntimeException}.
     * 
     * @see com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView#writeSearchResults(com.atlassian.jira.issue.search.SearchRequest, com.atlassian.jira.plugin.searchrequestview.SearchRequestParams, java.io.Writer)
     */
    public void writeSearchResults(final SearchRequest searchRequest, final SearchRequestParams searchRequestParams, final Writer writer)
    {
        final Map<String, Object> defaultParams = JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);
 
        try
        {
            writer.write(descriptor.getHtml("header"));
 
            final Searcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
            final HashMap<String, Object> issueParams = new HashMap<String, Object>(defaultParams);
 
            final DocumentHitCollector hitCollector = new IssueWriterHitCollector(searcher, writer, issueFactory)
            {
                protected void writeIssue(Issue issue, Writer writer) throws IOException
                {
                    //put the current issue into the velocity context and render the single issue view
                    issueParams.put("issue", issue);
                    writer.write(descriptor.getHtml("singleissue", issueParams));
                }
            };

            searchProvider.searchAndSort(searchRequest.getQuery(), authenticationContext.getLoggedInUser(), hitCollector, searchRequestParams.getPagerFilter());
            
            writer.write(descriptor.getHtml("footer"));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        catch (SearchException e)
        {
            throw new RuntimeException(e);
        }
    }
}
