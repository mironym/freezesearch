There are no unit tests right now because the whole plugin is just one class FreezeSearchRequestHtmlView which implements AbstractSearchRequestView.

I truly believe there is nothing to unit test right now, unless I on purpose change the FreezeSearchRequestHtmlView (introduce some methods) 
so they can be unit tested. Also, mocking of this single class that is part of the Jira plugin mechanisms would require more work than it is worth in my 
opinion.

Note that the plugin is tested by the integration test it.FreezeSearchPluginTest that basically goes through the whole workflow corresponding to the whole functionality.