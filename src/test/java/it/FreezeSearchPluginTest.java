/*
   Copyright 2012 Miroslav Hornik (Mironym)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package it;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.jira.functest.framework.FuncTestCase;

/**
 * This is the integration test of the Freeze Search Plugin. It simulates the simple workflow from searching
 * to invoking of the Freeze Search action and checks if the resulting link can be open and has the expected
 * "frozen" form (key in ...).
 * 
 * It relies on a pre-defined data - two bugs TEST-1 and TEST-2 in a Test project.
 * 
 * @author Miroslav Hornik
 *
 */
public class FreezeSearchPluginTest extends FuncTestCase {
	
	/**
	 * Restores the data needed for the test (from backup FreezeSearchPluginTestData.xml)
	 * and verifies whether they are correctly loaded by asserting the presence of both testing issues.
	 * 
	 * @see com.atlassian.jira.functest.framework.FuncTestCase#setUpTest()
	 */
	@Before
	public void setUpTest() {
		administration.restoreData("FreezeSearchPluginTestData.xml");
		
		//check if the restore was successful, we assume we are logged in, it seems so
		navigation.issueNavigator().displayAllIssues();
		assertBothBugsFound();
	}
	
	/**
	 * Test of the basic workflow of the Freeze Search functionality:
	 * 1. search for "status=Open" -> the result should be the both bugs
	 * 2. invoke "Freeze Search" action from the Issues Navigator Views menu
	 * 3. check if the resulting html page looks as expected (contains certain strings and also the frozen link leading to the search)
	 * 4. click on the frozen search link
	 * 5. verify if the search JQL Query is in form of "key in " and if there are present both issues.
	 * 
	 * This method assumes that admin is already logged in and that after it ends, it is logged out.
	 */
	@Test
	public void testBasicFunctionality() {
		navigation.issueNavigator().createSearch("status=Open");
		
		assertBothBugsFound();
		
		tester.clickLinkWithText("Freeze Search");
		
		text.assertTextPresent("Frozen search:");
		text.assertTextPresent("link to Jira");
		text.assertTextPresent("<a href=\"../../../secure/IssueNavigator.jspa?reset=true&jqlQuery=key+in+(TEST-2%2C+TEST-1%2C+null)\">");
		
		tester.clickLinkWithText("link to Jira");
		
		assertBothBugsFound();
		
		text.assertTextPresent("key in (TEST-2, TEST-1, null)");
	}
	
	/**
	 * Utility method that looks for the summaries of both bugs on a page. Used to check if
	 * the current search returned both of them.
	 */
	private void assertBothBugsFound() {
		text.assertTextPresent("Test bug 1");
		text.assertTextPresent("Test bug 2");
	}
}
